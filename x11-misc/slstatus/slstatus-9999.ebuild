# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6
inherit git-r3 eutils savedconfig toolchain-funcs

DESCRIPTION="slstatus is a suckless and lightweight status monitor for window managers"
HOMEPAGE="https://github.com/drkhsh/slstatus"
EGIT_REPO_URI="https://github.com/drkhsh/slstatus.git"

LICENSE="ISC"
SLOT="0"
KEYWORDS=""
IUSE="alsa"

DEPEND="
	x11-libs/libX11
	alsa? ( media-libs/alsa-lib )
"
RDEPEND="${DEPEND}"


src_prepare() {
	use alsa && epatch ${FILESDIR}/00-alsa-vol.diff
	epatch ${FILESDIR}/01-usleep.diff
	epatch ${FILESDIR}/02-wifi-blocks.diff

	sed -i \
		-e "s/CFLAGS = -std=c99 -pedantic -Wno-unused-function -Wall -Wextra -Os/CFLAGS += -std=c99 -pedantic -Wno-unused-function -Wall -Wextra -Wno-unused-result -Wno-array-bounds/" \
		-e "/^LDFLAGS/{s|=|+=|g;s|-s ||g}" \
		-e "s@/usr/X11R6/include@${EPREFIX}/usr/include/X11@" \
		-e "s@/usr/X11R6/lib@${EPREFIX}/usr/lib@" \
		-e "s@-I/usr/include@@" -e "s@-L/usr/lib@@" \
		config.mk || die

	restore_config config.h
	eapply_user
}

src_compile() {
	emake CC=$(tc-getCC) slstatus
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" install

	dodoc README.md

	save_config config.h
}

pkg_postinst() {
	einfo "This ebuild has support for user defined configs"
	einfo "Please read this ebuild for more details and re-emerge as needed"
	einfo "if you want to add or remove functionality for ${PN}"
}
