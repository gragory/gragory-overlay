# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PYTHON_COMPAT=( python2_7 )
inherit distutils-r1 eutils


MY_PN=PythonTidy

DESCRIPTION="Cleans up, regularizes, and reformats the text of Python scripts"
HOMEPAGE="http://pypi.python.org/pypi/PythonTidy/"
SRC_URI="http://lacusveris.com/${MY_PN}/${MY_PN}-${PV}.python"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_unpack() {
	cp "${DISTDIR}"/${A} "${WORKDIR}"/${PN}
}

src_prepare() {
	epatch ${FILESDIR}/00-disable-SHEBANG-and-CODING_SPEC.diff
	eapply_user
}

src_compile() {
	return
}

src_install() {
	python_foreach_impl python_newscript "${WORKDIR}"/${PN} ${PN}
	python_foreach_impl python_optimize
}

