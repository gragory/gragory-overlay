# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit perl-module

DESCRIPTION="Simple grep and pretty output for json in each files or standard input."
HOMEPAGE="https://bitbucket.org/gragory/app-yajg"
SRC_URI="https://bitbucket.org/gragory/app-yajg/get/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="|| ( Artistic GPL-1 GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-perl/JSON-XS"

src_unpack() {
	unpack ${A}
	mv ${WORKDIR}/*-${PN}-* ${S}
}

