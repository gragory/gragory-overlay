# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 linux-mod systemd multilib

DESCRIPTION="A Virtual Tape & Library system"
HOMEPAGE="https://sites.google.com/site/linuxvtl2/"
EGIT_REPO_URI="https://github.com/markh794/mhvtl"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="sys-libs/zlib"
RDEPEND="sys-apps/sg3_utils
	>=dev-lang/perl-5.24.0"

pkg_setup() {
	MODULE_NAMES="mhvtl(kernel/drivers/scsi:${S}/kernel:${S}/kernel)"
	BUILD_TARGETS="default"
	BUILD_PARAMS="KDIR=${KERNEL_DIR} EXTRA_CFLAGS=-DHAVE_UNLOCKED_IOCTL"
	linux-mod_pkg_setup
}

src_prepare() {
	sed -e '/ldconfig/d' -e '/systemctl/d' -i Makefile || die "patching Makefile failed"
	echo 'libvtlscsi.so: CLFLAGS += -Wl,-soname,libvtlscsi.so' >> usr/Makefile
	echo 'libvtlcart.so: CLFLAGS += -Wl,-soname,libvtlcart.so' >> usr/Makefile
	echo '.NOTPARALLEL:' >> usr/Makefile
	eapply_user
}

src_compile() {
	linux-mod_src_compile
	emake
}

src_install() {
	linux-mod_src_install
	emake DESTDIR="${D}" \
		LIBDIR="/usr/$(get_libdir)" \
		SYSTEMD_DIR="$(systemd_get_systemunitdir)" \
		SYSTEMD_GENERATOR_DIR=$(systemd_get_systemgeneratordir) \
		install
	newinitd "${FILESDIR}/mhvtl-modules.initd" mhvtl-modules
	newinitd "${FILESDIR}/mhvtl.initd" mhvtl
	newbin "${FILESDIR}/mhvtl-daemon.pl" mhvtl-daemon
}

