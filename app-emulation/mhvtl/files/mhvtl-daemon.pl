#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use mro;
use v5.24;

package main;

exit App::Mhvtl->main(@ARGV);

################################################################################

package App::Daemon;

use Fcntl qw(:flock);
use Getopt::Long qw(GetOptionsFromArray :config bundling);
use Sys::Syslog qw(:standard :macros);
use File::Spec;

use constant PIDFILE_DIR => File::Spec->catfile(File::Spec->rootdir(), qw(var run));

sub main {
    my $class = shift;
    my $self  = bless {
        name => $class->name,
        opt  => $class->getopt_default,
    }, $class;

    $self->getopt(@_);
    if ($self->{opt}->{help}) {
        $self->print_usage;
        return 0;
    }

    $self->init_log;
    $self->init;
    $self->check;

    return 0 if $self->{opt}->{check};

    App::Daemon::Utils::daemonize() unless $self->{opt}->{foreground};

    $self->write_pid_file;
    $self->notice("started");
    $self->run;
    $self->notice("finished");

    return 0;
}

sub DESTROY {
    closelog();
}

sub name { (ref($_[0]) || $_[0]) =~ tr/:/_/r; }

sub getopt_config { qw(
      verbose|v!
      pidfile|p=s
      foreground|f!
      help|h!
      check|c!
      ) }

sub getopt_default {
    my ($class) = @_;
    return {
        verbose    => '',
        help       => '',
        pidfile    => File::Spec->catfile(PIDFILE_DIR, $class->name . ".pid"),
        foreground => '',
        check      => '',
    };
}

sub short_usage {"$0 [-hvfc] [-p pidfile]"}

sub getopt_usage {
    return (
        ["verbose",    "v", "print external messages"],
        ["help",       "h", "show this message and exit"],
        ["pidfile",    "p", "specify path to pidfile"],
        ["foreground", "f", "run in foreground"],
        ["check",      "c", "check only"],
    );
}

sub print_usage {
    my ($class) = @_;
    say "usage: " . $class->short_usage;
    say "options: ";
    my @opts = sort { $a->[0] cmp $b->[0] } $class->getopt_usage;
    my $l    = 0;
    for (map { length $_->[0] } @opts) {
        $l = $_ if $_ > $l;
    }
    for (sort { $a->[0] cmp $b->[0] } @opts) {
        printf("  --%-${l}s  -%s    %s\n", @$_);
    }
}

sub getopt {
    my $self = shift;
    my $argv = \@_;
    GetOptionsFromArray($argv, $self->{opt}, $self->getopt_config)
      or die "Error in command line arguments\nUsage "
      . $self->short_usage
      . "\n";
}

sub init_log {
    my ($self) = @_;
    openlog($self->{name}, "ndelay,pid", LOG_DAEMON);
    $SIG{__WARN__} = sub { syslog(LOG_WARNING, "@_"); };
    $SIG{__DIE__}  = sub { syslog(LOG_ERR,     "@_"); };
    setlogmask(LOG_UPTO(LOG_NOTICE)) unless $self->{opt}->{verbose};
}

sub init { }

sub check { }

sub write_pid_file {
    my ($self) = @_;
    my $pidfile = $self->{opt}->{pidfile};
    open(my $fh, ">", $pidfile)
      or die "open $pidfile: $!\n";
    flock($fh, LOCK_EX | LOCK_NB) or die "already running\n";
    say $fh $$;
    select((select($fh), $| = 1)[0]);
    $self->{pidfile_fh} = $fh;
}

sub info {
    my $self = shift;
    return unless $self->{opt}->{verbose};
    syslog(LOG_INFO, @_);
}

sub notice {
    my $self = shift;
    syslog(LOG_NOTICE, @_);
}

sub run {...}

################################################################################

package App::Daemon::Utils;

use POSIX qw(setsid :fcntl_h);
use File::Spec;

sub dup2($$) {
    my ($fd1, $fd2) = @_;
    return unless $fd1 and $fd2 and $fd1 >= 0 and $fd2 >= 0;
    return POSIX::dup2($fd1, $fd2);
}

sub daemonize() {
    # first fork
    my $pid = fork() // die "fork: $!\n";
    exit 0 if $pid;
    # become leader of new session
    setsid() or die "setsid: $!\n";
    # second fork - ensure we are not session leader
    $pid = fork() // die "fork: $!\n";
    exit 0 if $pid;

    umask(0);
    chdir("/");

    state $devnull = File::Spec->devnull();
    my $fd = POSIX::open($devnull, O_RDWR) // die "open $devnull: $!\n";
    dup2($fd, fileno(STDIN));
    dup2($fd, fileno(STDOUT));
    dup2($fd, fileno(STDERR));
    POSIX::close($fd);
}

################################################################################

package App::Mhvtl;

use POSIX qw(sigprocmask sigsuspend sigaction :signal_h :sys_wait_h);
use Time::HiRes qw(usleep gettimeofday tv_interval);

use base qw(App::Daemon);

use constant VTL_CONF_PATH      => "/etc/mhvtl/device.conf";
use constant VTL_LIBRARY        => "/usr/bin/vtllibrary";
use constant VTL_TAPE           => "/usr/bin/vtltape";
use constant VTL_CMD            => "/usr/bin/vtlcmd";
use constant SIGNALS            => (SIGINT, SIGTERM, SIGHUP, SIGCHLD);
use constant WAITPID_TIMEOUT_US => 100_000;
use constant KILL_TIMEOUT_S     => 5;

sub name {"mhvtl-daemon"}

sub init {
    my ($self) = @_;
    $self->parse_config;
}

sub check {
    my ($self) = @_;
    for my $file (VTL_LIBRARY, VTL_TAPE, VTL_CMD) {
        -x $file or die "$file: $!\n";
    }
}

sub run {
    my ($self) = @_;
    $self->init_signal;
    my $ret = eval {
        $self->start_vtl;
        $self->main_loop;
        1;
    };
    my $error = $@;
    $self->stop_vtl;
    die "FAILED: $error\n" unless $ret;
}

sub parse_config {
    my ($self) = @_;
    my $config = $self->{config} = {};
    open my $fh, '<', VTL_CONF_PATH
      or die "Failed to open conf file: $!\n";
    while (<$fh>) {
        chomp;
        next unless $_;    # empty line
        next if m/^#/;     # comment line
        next if m/^\s/;    # middle of record
        my ($type, $queueid) = m/^(Library|Drive): ([0-9]++)/;
        next unless defined $type and $queueid;
        state $type_map = {
            Library => VTL_LIBRARY,
            Drive   => VTL_TAPE,
        };
        $config->{ $type_map->{$type} }->{$queueid} = 1;
    }
    close $fh;
}

sub init_signal {
    my ($self) = @_;

    $SIG{PIPE} = "IGNORE";

    my $sighash    = $self->{signal} = {};
    my $sighandler = sub { $sighash->{ $_[0] } = 1; };
    my $sigset     = POSIX::SigSet->new((SIGNALS));
    my $flags      = SA_NOCLDSTOP;
    my $sigaction  = POSIX::SigAction->new($sighandler, $sigset, $flags);
    for my $sig (SIGNALS) {
        sigaction($sig, $sigaction) or die "sigaction $sig: $!\n";
    }
}

sub start_vtl_daemon {
    my ($self, $daemon, $queueid) = @_;
    my @cmd = ($daemon, "-F", "-q", $queueid);
    push @cmd, "-v" if $self->{opt}->{verbose};
    $self->info("start vtl daemon: @cmd");
    my $pid = fork() // die "fork: $!\n";
    if ($pid) {
        my $order = scalar keys $self->{child}->%*;
        # parent
        my $info = {
            daemon  => $daemon,
            cmd     => join(" ", @cmd),
            queueid => $queueid,
            pid     => $pid,
            order   => $order
        };
        $self->{child}->{$pid} = $info;
        return $self;
    }
    else {
        # child
        exec @cmd or die "exec: $!\n";
    }
}

sub send_exit {
    my ($self, $queueid) = @_;
    return system(VTL_CMD, $queueid, "exit") == 0;
}

sub stop_vtl_daemon {
    my ($self, $info) = @_;
    $self->info("stop vtl daemon, cmd = '$info->{cmd}', pid = $info->{pid}");
    unless (kill 0 => $info->{pid}) {
        warn "child $info->{pid} was already stopped\n";
        return;
    }
    unless ($self->send_exit($info->{queueid})) {
        warn VTL_CMD . " failed, sending SIGKILL to $info->{pid}\n";
        kill -9 => $info->{pid};
    }

    my $t = [gettimeofday];
    while (waitpid($info->{pid}, WNOHANG) != $info->{pid}) {
        usleep(WAITPID_TIMEOUT_US);
        if (tv_interval($t) >= KILL_TIMEOUT_S)
        {
            warn "$info->{pid} exit timeout, sending SIGKILL\n";
            kill -9 => $info->{pid};
        }
    }
    my $st     = $?;
    my $logmsg = "'$info->{cmd}', pid = $info->{pid}, exited with status $st";
    if ($st == 0) {
        $self->info($logmsg);
    }
    else {
        warn "$logmsg\n";
    }
}

sub start_vtl {
    my ($self) = @_;
    for my $daemon (VTL_LIBRARY, VTL_TAPE) {
        for my $queueid (keys $self->{config}->{$daemon}->%*) {
            $self->start_vtl_daemon($daemon, $queueid);
        }
    }
}

sub stop_vtl {
    my ($self) = @_;
    for my $info (sort { $b->{order} <=> $a->{order} } values $self->{child}->%*) {
        $self->stop_vtl_daemon($info);
    }
}

sub main_loop {
    my ($self) = @_;
    while (1) {
        my $sig = $self->get_signal;
        next unless %$sig;
        $self->info("Got signals: @{[keys %$sig]}");
        if ($sig->{INT} or $sig->{TERM}) {
            last;
        }
        elsif ($sig->{CHLD}) {
            while ((my $pid = waitpid(-1, WNOHANG)) > 0) {
                my $st   = $?;
                my $info = delete $self->{child}->{$pid}
                  or next;
                die "cmd = '$info->{cmd}', pid = $info->{pid}, unexpected exit with status $st\n";
            }
        }
        elsif ($sig->{HUP}) {
            $self->notice("reload daemons");
            $self->parse_config;
            my $config = $self->{config};
            my %reloaded;
            for my $info (sort { $b->{order} <=> $a->{order} } values $self->{child}->%*) {
                my $daemon  = $info->{daemon};
                my $queueid = $info->{queueid};
                if ($daemon eq VTL_LIBRARY and $config->{$daemon}->{$queueid}) {
                    $self->info("Sending SIGHUP to $info->{pid}");
                    kill -1 => $info->{pid};
                    $reloaded{$queueid} = 1;
                }
                else {
                    $self->stop_vtl_daemon($info);
                    delete $self->{child}->{ $info->{pid} };
                }
            }
            for my $daemon (VTL_LIBRARY, VTL_TAPE) {
                for my $queueid (keys $self->{config}->{$daemon}->%*) {
                    next if $reloaded{$queueid};
                    $self->start_vtl_daemon($daemon, $queueid);
                }
            }
        }
    }
}

sub get_signal {
    my ($self) = @_;
    my $sigset = POSIX::SigSet->new((SIGNALS));
    my $oldset = POSIX::SigSet->new();
    sigprocmask(SIG_BLOCK, $sigset, $oldset);
    my $sighash = $self->{signal};
    unless (%$sighash) {
        sigsuspend($oldset);
    }
    my %ret = %$sighash;
    %$sighash = ();
    sigprocmask(SIG_SETMASK, $oldset);
    return \%ret;
}

